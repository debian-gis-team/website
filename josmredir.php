<?php
$url = "http://josm.openstreetmap.de/version";

$tested_re = "/josm-tested.jar: (\d+)/";
$latest_re = "/josm-latest.jar: (\d+)/";

$svnrepo = "http://josm.openstreetmap.de/svn/trunk";
$i18nrepo = "http://svn.openstreetmap.org/applications/editors/josm/i18n/";

$type = "tested";
$re = $tested_re;

if (isset($_GET["type"])) {
    switch ($_GET["type"]) {
        case "latest":
            $type = "latest";
            $re = $latest_re;
            break;
        default:
            break;
    }
}

?>
<!DOCTYPE html>
<html>
<head>
  <title>JOSM Redirector</title>
  <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
</head>

<body>

<h1>JOSM Redirector</h1>

<p>
  This page is mainly meant to be used with
  <span style="font-family: monospace;"><a href="http://manpages.debian.org/uscan">uscan(1)</a></span>.
</p>
<p>
  JOSM releases:
</p>
<ul>
  <li><a href="<?= $_SERVER["PHP_SELF"] ?>?type=tested">tested</a></li>
  <li><a href="<?= $_SERVER["PHP_SELF"] ?>?type=latest">latest</a></li>
</ul>
<?php

$handle = @fopen($url, "r");
if ($handle) {
    while (!feof($handle)) {
        $buffer = fgets($handle);
        if (preg_match($re, $buffer, $matches)) {
            $version = $matches[1];
?>
<a href='<?= $_SERVER["PHP_SELF"] ?>?rev=<?= $version ?>'>JOSM <?= $type ?> <?= $version ?></a>
<?php
        }
    }
    fclose($handle);
}

if (isset($_GET["rev"]) && preg_match("/^\d+$/", $_GET["rev"])) {
?>
<hr />
To get the source for JOSM <?= $_GET["rev"] ?> run:
<pre>
SVNREV=<?= $_GET["rev"] ?> make -f debian/rules get-orig-source
</pre>
<?php
}

?>
<hr />
<address>
  For any issue contact the <a href="mailto:debian-gis@lists.debian.org">Debian GIS mailinglist</a>.<br/>
  <small>
    Source code available in the website git repository:
    <a href="https://anonscm.debian.org/cgit/pkg-grass/website.git/tree/josmredir.php">https://anonscm.debian.org/cgit/pkg-grass/website.git/tree/josmredir.php</a>.
  </small>
</address>

</body>
</html>
