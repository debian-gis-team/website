PUBLISHHOST=alioth.debian.org
PUBLISHDIR=/home/groups/pkg-grass/htdocs

all: policy

policy: valid spellcheck
	xsltproc \
		--param toc.section.depth 3 \
		--param toc.max.depth 2 \
		--param generate.section.toc.level 2 \
		--param use.id.as.filename 1 \
		--param make.valid.html 1 \
		--stringparam html.stylesheet policy.css \
		--stringparam base.dir policy/ \
		/usr/share/xml/docbook/stylesheet/docbook-xsl/xhtml/chunk.xsl \
		policy.xml 2>/dev/null

clean:
	rm -f docbook.css policy/*.html

valid:
	xmllint --valid --noout policy.xml

spellcheck:
	codespell policy.xml

spellcheck-fix:
	codespell -w -s policy.xml

publish: policy
	rsync -av .gitignore .htaccess * $(PUBLISHHOST):$(PUBLISHDIR) || true
	ssh $(PUBLISHHOST) "chmod -R g+w $(PUBLISHDIR)/*" || true
